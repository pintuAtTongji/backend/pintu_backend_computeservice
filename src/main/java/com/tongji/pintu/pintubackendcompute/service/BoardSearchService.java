package com.tongji.pintu.pintubackendcompute.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tongji.pintu.pintubackendcompute.domain.Board;
import com.tongji.pintu.pintubackendcompute.domain.Key;
import com.tongji.pintu.pintubackendcompute.mapper.test1.BoardMapper;
import java.util.List;

@Service("boardSearchService")
public class BoardSearchService {
    @Autowired
    private BoardMapper boardMapper;
    public List<Board> searchWithTitle(Key key) {return boardMapper.searchWithTitle(key);}

    public List<Board> searchWithContent(Key key) {return boardMapper.searchWithContent(key);}
}
