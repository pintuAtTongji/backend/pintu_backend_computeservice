package com.tongji.pintu.pintubackendcompute.service;

import com.alibaba.fastjson.JSONObject;
import com.tongji.pintu.pintubackendcompute.domain.Key;
import com.tongji.pintu.pintubackendcompute.domain.UserInformation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tongji.pintu.pintubackendcompute.mapper.test2.UserMapper;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

@Service("UserSearchService")
public class UserSearchService {
    @Autowired
    private UserMapper userMapper;

   public List<JSONObject> searchWithName(Key key) {
       List<UserInformation> userInformationResult =new LinkedList<>();
       userInformationResult  =userMapper.searchWithName(key);
       List<JSONObject> userResultJSON = new LinkedList<>();
       for (int i=0;i<userInformationResult.size();i++)
       {
           JSONObject userJSON = new JSONObject(
                  new LinkedHashMap()
           );
           userJSON.put("userId",userInformationResult.get(i).getUser_id());
           userJSON.put("name",userInformationResult.get(i).getName());
           userJSON.put("profilePhotoUrl",userInformationResult.get(i).getProfile_photo_url());
           userJSON.put("sexual",userInformationResult.get(i).getSexual());
           userJSON.put("university",userInformationResult.get(i).getUniversity());
           userJSON.put("college",userInformationResult.get(i).getCollege());
           userJSON.put("major",userInformationResult.get(i).getMajor());
           userJSON.put("introduction",userInformationResult.get(i).getIntroduction());
           userJSON.put("age",userInformationResult.get(i).getAge());
           userJSON.put("phoneNumber",userInformationResult.get(i).getPhone_number());
           userJSON.put("education",userInformationResult.get(i).getEducation());
           List<String> tag=userMapper.getTag(userInformationResult.get(i).getUser_id());
           if (tag.size()==0)
               tag=null;
           userJSON.put("tag",tag);
           userResultJSON.add(userJSON);
       }
       return userResultJSON;}
}
