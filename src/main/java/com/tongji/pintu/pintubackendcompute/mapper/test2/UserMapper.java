package com.tongji.pintu.pintubackendcompute.mapper.test2;

import com.tongji.pintu.pintubackendcompute.domain.Key;
import com.tongji.pintu.pintubackendcompute.domain.UserInformation;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserMapper {
    List<UserInformation> searchWithName (Key key);
    List<String> getTag (int id);
}
