package com.tongji.pintu.pintubackendcompute.mapper.test1;

import com.tongji.pintu.pintubackendcompute.domain.Board;
import com.tongji.pintu.pintubackendcompute.domain.Key;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component
public interface BoardMapper {
    Board getBoard(int id);

    List<Board> searchWithTitle (Key key);

    List<Board> searchWithContent (Key key);
}

