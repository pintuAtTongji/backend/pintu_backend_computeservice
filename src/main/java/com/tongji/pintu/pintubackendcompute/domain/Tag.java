package com.tongji.pintu.pintubackendcompute.domain;

import io.swagger.annotations.ApiModelProperty;

public class Tag {
    @ApiModelProperty(value = "标签id", position = 1)
    private Integer id;

    @ApiModelProperty(value = "标签名", position = 2)
    private String name;

    @ApiModelProperty(value = "标签描述", position = 3)
    private String description;

    public Tag() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
