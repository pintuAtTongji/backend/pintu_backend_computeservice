package com.tongji.pintu.pintubackendcompute.domain;

import io.swagger.annotations.ApiModelProperty;

import java.util.List;

public class UserInformation {
    @ApiModelProperty(value = "用户id", position = 1)
    private Integer user_id;

    @ApiModelProperty(value = "昵称", position = 2)
    private String name;



    @ApiModelProperty(value = "头像路径", position = 3)
    private String profile_photo_url;

    @ApiModelProperty(value = "性别，具体数字映射关系前端做处理", position = 4)
    private Integer sexual;

    @ApiModelProperty(value = "学校", position = 5)
    private String university;

    @ApiModelProperty(value = "学院", position = 6)
    private String college;

    @ApiModelProperty(value = "专业", position = 7)
    private String major;

    @ApiModelProperty(value = "个人介绍", position = 8)
    private String introduction;

    @ApiModelProperty(value = "年龄", position = 9)
    private Integer age;

    @ApiModelProperty(value = "手机号", position = 10)
    private String phone_number;

    @ApiModelProperty(value = "教育经历", position = 11)
    private String education;



    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public Integer getUser_id() {
        return user_id;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }

    public String getProfile_photo_url() {
        return profile_photo_url;
    }

    public void setProfile_photo_url(String profile_photo_url) {
        this.profile_photo_url = profile_photo_url;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public Integer getSexual() {
        return sexual;
    }

    public void setSexual(Integer sexual) {
        this.sexual = sexual;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }



    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }
}
