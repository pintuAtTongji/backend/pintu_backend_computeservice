package com.tongji.pintu.pintubackendcompute.domain;

import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class Board {
    @ApiModelProperty(value = "图板id", position = 1)
    private Integer id;

    @ApiModelProperty(value = "发起者id", position = 2)
    private Integer user_id;

    @ApiModelProperty(value = "标题", position = 3)
    private String title;

    @ApiModelProperty(value = "内容", position = 4)
    private String content;

    @ApiModelProperty(value = "参与人数", position = 5)
    private Integer user_count;

    @ApiModelProperty(value = "创建时间",example = "2019-10-10 10:10:10",position = 6)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date gmt_create;

    @ApiModelProperty(value = "图片", position = 7)
    private String related_image;


    public Board(Integer id, Integer userId, String title, String content, Integer userCount, Date gmtCreate,String related_image) {
        this.id = id;
        this.user_id= userId;
        this.title = title;
        this.content = content;
        this.user_count = userCount;
        this.gmt_create = gmtCreate;
        this.related_image=related_image;
    }

    public Board() {
    }


    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content =content;
    }

    public Integer getUser_count() {
        return user_count;
    }

    public void setUser_count(Integer user_count) {
        this.user_count = user_count;
    }

    public String getRelated_image() {
        return related_image;
    }

    public void setRelated_image(String related_image) {
        this.related_image = related_image;
    }


    public Date getGmt_create() {
        return gmt_create;
    }

    public void setGmt_create(Date gmt_create) {
        this.gmt_create = gmt_create;
    }


}
