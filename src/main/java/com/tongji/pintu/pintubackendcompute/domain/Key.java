package com.tongji.pintu.pintubackendcompute.domain;

import io.swagger.annotations.ApiModelProperty;

public class Key {
    @ApiModelProperty(value = "搜索键", position = 1)
    private String key;
    public Key() {
    }

    public Key(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
