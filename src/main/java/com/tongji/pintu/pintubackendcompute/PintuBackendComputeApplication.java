package com.tongji.pintu.pintubackendcompute;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {""})
public class PintuBackendComputeApplication {

	public static void main(String[] args) {
		SpringApplication.run(PintuBackendComputeApplication.class, args);
	}

}
