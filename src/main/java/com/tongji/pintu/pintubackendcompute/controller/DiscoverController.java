package com.tongji.pintu.pintubackendcompute.controller;

import com.tongji.pintu.pintubackendcompute.domain.UserInformation;
import com.tongji.pintu.pintubackendcompute.utils.Response;
import com.tongji.pintu.pintubackendcompute.utils.ResponseUtils;
import com.tongji.pintu.pintubackendcompute.domain.Board;
import com.tongji.pintu.pintubackendcompute.domain.Key;
import com.tongji.pintu.pintubackendcompute.service.BoardSearchService;
import com.tongji.pintu.pintubackendcompute.service.UserSearchService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSONObject;
import com.tongji.pintu.pintubackendcompute.service.DiscoverService;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


@RestController
@RequestMapping("/discover")
public class DiscoverController {
    @Autowired
    private BoardSearchService boardSearchService;
    @Autowired
    private UserSearchService userSearchService;
    @Autowired
    private DiscoverService discoverService;

    @ApiOperation("通过用户id推荐用户与图板")
    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public Response<List<JSONObject>> discover(@PathVariable("id") Integer id){
        Key key=new Key("");
        List<Board> boardResult=boardSearchService.searchWithTitle(key);
        List<JSONObject> userInformationResult=userSearchService.searchWithName(key);
        boardResult=discoverService.getRandomList(boardResult,20);
        userInformationResult=discoverService.getRandomList(userInformationResult,20);
        JSONObject boardJSON = new JSONObject();
        JSONObject userJSON = new JSONObject();
        userJSON.put("users",userInformationResult);
        boardJSON.put("board",boardResult);
        List<JSONObject> resultJSON =new ArrayList<>();
        resultJSON.add(userJSON);
        resultJSON.add(boardJSON);
        return ResponseUtils.success(resultJSON);
    }

    @ApiOperation("推荐用户与图板")
    @RequestMapping(value = "/",method = RequestMethod.GET)
    public Response<List<JSONObject>> discover(){
        Key key=new Key("");
        List<Board> boardResult=boardSearchService.searchWithTitle(key);
        List<JSONObject> userInformationResult=userSearchService.searchWithName(key);
        boardResult=discoverService.getRandomList(boardResult,20);
        userInformationResult=discoverService.getRandomList(userInformationResult,20);
        JSONObject boardJSON = new JSONObject();
        JSONObject userJSON = new JSONObject();
        userJSON.put("users",userInformationResult);
        boardJSON.put("board",boardResult);
        List<JSONObject> resultJSON =new ArrayList<>();
        resultJSON.add(userJSON);
        resultJSON.add(boardJSON);
        return ResponseUtils.success(resultJSON);
    }
}
