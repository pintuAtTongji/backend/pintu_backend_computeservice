package com.tongji.pintu.pintubackendcompute.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.tongji.pintu.pintubackendcompute.domain.UserInformation;
import com.tongji.pintu.pintubackendcompute.utils.Response;
import com.tongji.pintu.pintubackendcompute.utils.ResponseUtils;
import com.tongji.pintu.pintubackendcompute.domain.Board;
import com.tongji.pintu.pintubackendcompute.domain.Key;
import com.tongji.pintu.pintubackendcompute.service.BoardSearchService;
import com.tongji.pintu.pintubackendcompute.service.UserSearchService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSONObject;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import com.tongji.pintu.pintubackendcompute.mapper.test1.BoardMapper;
import com.tongji.pintu.pintubackendcompute.mapper.test2.UserMapper;


@RestController
public class SearchController {
    @Autowired
    private BoardSearchService boardSearchService;
    @Autowired
    private UserSearchService userSearchService;
    @Autowired
    private UserMapper userMapper;


    @ApiOperation("搜索用户与图板")
    @RequestMapping(value = "/search",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "key",paramType = "body",dataType = "Key"),
    })

    public Response<List<JSONObject>> search(@RequestBody Key key){
        List<Board> boardResult=boardSearchService.searchWithTitle(key);
        boardResult.addAll(boardSearchService.searchWithContent(key));

        JSONObject boardJSON = new JSONObject();
        List<JSONObject> userResultJSON = new LinkedList<>();
        userResultJSON =userSearchService.searchWithName(key);
       // userJSON.put("users",userInformationResult);//userJSON add tag
        JSONObject jsonObject = new JSONObject();
        boardJSON.put("board",boardResult);
        jsonObject.put("user",userResultJSON);
        List<JSONObject> resultJSON =new ArrayList<>();
        resultJSON.add(jsonObject);
        resultJSON.add(boardJSON);
        return ResponseUtils.success(resultJSON);
    }

}

